package archivos;

import archivos.gui.FileInfo;
import archivos.gui.FileManager;
import archivos.gui.TranslatorGUI;

import javax.swing.*;

public class ClasePrincipal
{
    public static void main(String args[])
    {
        //JFrame frame = new JFrame("FileInfo");
        //JFrame frame = new JFrame("Translator");
        JFrame frame = new JFrame("File Manager");
        frame.setContentPane(new FileManager().getMainPanel());
        //frame.setContentPane(new TranslatorGUI().getMainPanel());
        //frame.setContentPane(new FileInfo().getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(400, 600);
        //frame.setSize(280, 125);
        frame.setVisible(true);
        frame.setResizable(false);
    }
}
