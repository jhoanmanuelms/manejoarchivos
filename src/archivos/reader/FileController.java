package archivos.reader;

import javax.swing.*;
import java.io.*;

public class FileController
{
    protected File file;

    public FileController(String fileName)
    {
        file = new File(fileName);
    }

    public FileController(File file)
    {
        this.file = file;
    }

    public String getFileInfo()
    {
        StringBuilder fileInfo = new StringBuilder();
        if (file.exists())
        {
            fileInfo.append("\nRuta: " + file.getPath() + "\nRuta absoluta: " + file.getAbsolutePath());

            if (file.isFile())
            {
                try
                {
                    BufferedReader entrada = new BufferedReader(new FileReader(file));
                    fileInfo.append("\n\nCONTENIDO DEL ARCHIVO: \n");
                    String texto;

                    while ((texto = entrada.readLine()) != null)
                    {
                        fileInfo.append("\n" + texto);
                    }

                } catch (IOException ioe)
                {
                    JOptionPane.showMessageDialog(
                        null,
                        "Error al leer el archivo",
                        "ERROR EN ARCHIVO",
                        JOptionPane.ERROR_MESSAGE);
                    ioe.printStackTrace();
                }
            }
            else if (file.isDirectory())
            {
                fileInfo.append("\n\nCONTENIDO DE LA CARPETA:\n");

                for (String contenido : file.list())
                {
                    fileInfo.append(contenido + "\n");
                }
            }
        }
        else
        {
            fileInfo.append("La ruta especificada no apunta a ningun archivo o carpeta");
        }

        return fileInfo.toString();
    }

    public String getFileContent()
    {
        StringBuilder fileContent = new StringBuilder();
        try
        {
            RandomAccessFile fileReader = new RandomAccessFile(file, "r");
            String line = fileReader.readLine();

            while (line != null)
            {
                fileContent.append("\n").append(line);
                line = fileReader.readLine();
            }

        }
        catch (FileNotFoundException fne)
        {
            JOptionPane.showMessageDialog(
                null,
                "El archivo no existe",
                "ERROR EN ARCHIVO",
                JOptionPane.ERROR_MESSAGE);
            fne.printStackTrace();
        }
        catch (IOException ioe)
        {
            JOptionPane.showMessageDialog(
                null,
                "Error al leer el archivo",
                "ERROR EN ARCHIVO",
                JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();
        }

        return fileContent.toString();
    }
}
