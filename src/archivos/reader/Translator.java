package archivos.reader;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

public class Translator extends FileController
{
    private static final String TRANSLATION_DATABASE = "resources/input/data.txt";
    private RandomAccessFile fileReader;

    public Translator()
    {
        super(TRANSLATION_DATABASE);
        try
        {
            fileReader = new RandomAccessFile(file, "r");
        }
        catch (FileNotFoundException fne)
        {
            JOptionPane.showMessageDialog(
                null,
                "La base de datos de traduccion no fue encontrada",
                "Error Base Datos Traduccion",
                JOptionPane.ERROR_MESSAGE);
            fne.printStackTrace();
        }
    }

    public String translate(String word)
    {
        boolean translated = false;
        String translation = "Palabra No Registrada";

        try
        {
            String line;
            do
            {
                line = fileReader.readLine();
                String[] tupla = line.split("#");

                if (tupla[0].equals(word.toUpperCase()))
                {
                    translation = tupla[1];
                    translated = true;
                }
            }
            while(!translated || line == null);
        }
        catch (IOException ioe)
        {
            JOptionPane.showMessageDialog(
                null,
                "Error al leer la base de datos de palabras",
                "Error Base Datos Traduccion",
                JOptionPane.ERROR_MESSAGE);
            ioe.printStackTrace();
        }
        finally
        {
            try
            {
                fileReader.close();
            }
            catch (IOException ioe)
            {
                JOptionPane.showMessageDialog(
                    null,
                    "Error al cerrar la base de datos de palabras",
                    "Error Base Datos Traduccion",
                    JOptionPane.ERROR_MESSAGE);
                ioe.printStackTrace();
            }
        }

        return translation;
    }
}
