package archivos.gui;

import archivos.reader.FileController;

import javax.swing.*;

public class FileInfo
{
    private JPanel mainPanel;
    private JTextField nombreArchivo;
    private JScrollPane scrollPanel;
    private JTextArea infoArchivo;

    public FileInfo()
    {
        nombreArchivo.addActionListener(actionEvent -> getFileInfo());
    }

    public JPanel getMainPanel()
    {
        return mainPanel;
    }

    private void getFileInfo()
    {
        FileController fileController = new FileController(nombreArchivo.getText());
        infoArchivo.setText(fileController.getFileInfo());
    }
}
