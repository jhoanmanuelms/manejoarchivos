package archivos.gui;

import archivos.reader.Translator;

import javax.swing.*;

public class TranslatorGUI
{
    private JTextField spanish;
    private JTextField english;
    private JButton translate;
    private JPanel mainPanel;

    public TranslatorGUI()
    {
        spanish.addActionListener(actionEvent -> translate());
        translate.addActionListener(actionEvent -> translate());
    }

    private void translate()
    {
        english.setText(new Translator().translate(spanish.getText()));
    }

    public JPanel getMainPanel()
    {
        return mainPanel;
    }
}
