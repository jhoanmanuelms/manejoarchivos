package archivos.gui;

import archivos.reader.FileController;

import javax.swing.*;

public class FileManager
{
    private JPanel mainPanel;
    private JButton open;
    private JButton process;
    private JButton save;
    private JTextArea fileContent;
    private FileController fileController;

    public FileManager()
    {
        open.setIcon(new ImageIcon("resources/img/abrir.JPG"));
        open.addActionListener(actionEvent -> openFile());
    }

    private void openFile()
    {
        JFileChooser fileChooser = new JFileChooser();
        int selection = fileChooser.showOpenDialog(mainPanel);

        if (selection == JFileChooser.APPROVE_OPTION)
        {
            fileController = new FileController(fileChooser.getSelectedFile());
            fileContent.setText(fileController.getFileContent());
            process.setEnabled(true);
        }
    }

    public JPanel getMainPanel()
    {
        return mainPanel;
    }
}
